import AjaxService from 'ember-ajax/services/ajax';

export default AjaxService.extend({
    host : 'https://api.github.com/',
    headers :  {'Access-Control-Allow-Origin': 'http://localhost:4200',
                'Access-Control-Allow-Methods' : 'GET, PUT, POST, OPTIONS, DELETE',
                'Access-Control-Max-Age' : '2592000',
                'Accept' : 'application/json'
            }
});
