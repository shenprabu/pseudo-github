import Route from '@ember/routing/route';

export default Route.extend({

    afterModel: function() {
		// this._super();
		if(window.localStorage.getItem('access_token')) {
			console.log('to home page');
			this.transitionTo('home');
		}
		else {
			console.log('to login page');
			this.transitionTo('login');
		}
    }
    
});
