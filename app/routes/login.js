import Route from '@ember/routing/route';
import ENV from '../config/environment';

export default Route.extend({

    setupController: function(controller, model) {
        controller.set('client_id', ENV['client_id']);
    },

    beforeModel : function() {
        if(window.location.href.indexOf('?code=') != -1){
			var code = this.getUrlVars()['code'];
            ENV['code'] = code;
			if(code) {
                this.getAuthToken();
                
			}
        }
    },
    
    getUrlVars : function getUrlVars() {
		var vars = {};
		window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
    },

    getAuthToken : function() {

        $.ajax({
            url: 'https://github.com/login/oauth/access_token',
            type: 'post',
            data: {
                client_id : ENV['client_id'],
                client_secret : ENV['client_secret'],
                code : ENV['code']
            },
            headers: {
                'Accept' : 'application/json'
            },
            success: response => {
                console.log('response', response);
                window.localStorage.setItem('access_token', response['access_token']);
                this.transitionTo('home');
            },
            error: error => {
                console.log('error', error);
            }
        })
	}
});
