import Route from '@ember/routing/route';

export default Route.extend({

    beforeModel : function() {
        $.ajax({
			url : 'https://api.github.com/user',
			type : 'get',
            headers : {
				'Accept' : 'application/json',
                'Authorization' : 'token ' + window.localStorage.getItem('access_token')
			},
			success : response => {
				console.log('user', response);
				this.get('controller').set('user', response);
			},
			error : error => {
				console.log('error', error);
				alert("login failed !!");
				this.transitionTo('login');
			}
		});
	}
});
