import Controller from '@ember/controller';

export default Controller.extend({

    actions: {
        logout : function() {
            
            window.localStorage.setItem('access_token', '');
            window.localStorage.removeItem('access_token');

            // var browser = 
            // chrome.cookies.remove({
            //     url: 'github.com',
            //     name: "user_session"
            // });

            this.transitionToRoute('login');
        },

        getRepos: function(url, container) {
            
            $.ajax({
                url: url,
                type : 'get',
                success : response => {
                    console.log('response', response);
                    this.set(container, response);
                    this.set('isClicked', true);
                },
                error : error => {
                    console.log('error', error);
                }
            })
        },

        getContents: function(repo) {
            var url = repo.contents_url;
            if(url.indexOf('{+path}') != -1){
                url = url.substring(0, url.length - 7);
                Ember.set(repo, 'contents_url', url);
                console.log('url', url);
            }
            $.ajax({
                url: url,
                type : 'get',
                success : response => {
                    console.log('response', response);
                    Ember.set(repo, 'contents', response);
                    Ember.set(repo, 'isClicked', true);
                    // repo.set('contents', response);
                },
                error : error => {
                    console.log('error', error);
                }
            })
        }
    }
});
