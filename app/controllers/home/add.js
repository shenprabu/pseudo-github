import Controller from '@ember/controller';

export default Controller.extend({

    queryParams: ['contents_url','path'],
    isCaptured: false,
    fileName: 'image',
    message: 'image uploaded',
    
    actions: {
        close: function() {
            this.set('isCaptured', false);
            this.set('isVideo', false);
            this.set('error_msg', '');
            this.send('stopStream');
            this.transitionToRoute('home');
        },

        getUserMedia: function() {
            this.set('isVideo', true);
            var video = document.getElementById('video');
            var canvas = document.getElementById('canvas');
            this.set('video', video);
            canvas.style.display = 'none';
            video.style.display = 'block';
            if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({audio : false, video : {width: {exact: 640}, height: {exact: 480}}})
                .then(stream => {
                    video.srcObject = stream;
                }, error => {
                    console.log('error case', error);
                })
            } else {
                alert('This browser does not support this type of recording :(');
            }
        },

        capturePic: function() {
            var video = document.getElementById('video');
            var canvas = document.getElementById('canvas');
            canvas.style.display = 'block';
            video.style.display = 'none';
            canvas.getContext('2d').drawImage(video, 10, 10);
            this.send('stopStream');
            this.set('imageData', canvas.toDataURL());
            this.set('isCaptured', true);
            this.set('isVideo', false);
        },

        recapture: function() {
            this.set('isCaptured', false);
            this.send('getUserMedia');
        },

        stopStream: function() {
            var video = this.get('video');
            if(video && video.srcObject) {
                video.srcObject.getTracks().forEach(function(track) {
                    track.stop();
                });
            }
        },

        upload: function() {
            if(this.fileName && this.message) {
                this.set('error_msg', '');
                this.send('commitImage');
            }
            else {
                this.set('error_msg', 'Enter necessary values');
            }
        },

        commitImage: function() {
            var url = this.get('contents_url') + this.get('path');
            url = url.endsWith('/') ? url : url + '/';
            console.log('upload url', url + this.get('fileName') + '.jpg');
            $.ajax({
                url: url + this.get('fileName') + '.jpg',
                // url: this.get('contents_url') + this.get('path') + 'helloworld.txt',
                type : 'put',
                data : {
                    message: this.get('message'),
                    content : this.get('imageData')
                    // content : 'bXkgbmV3IGZpbGUgY29udGVudHM='
                },
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Authorization' : 'token ' + window.localStorage.getItem('access_token')
                },
                success : response => {
                    console.log('response', response);
                    alert(this.get('fileName') + 'uploaded.');
                },
                error : error => {
                    console.log('error', error);
                    alert('error while uploading file. Error message : ' + error['responseJSON']['message']);
                }
            })
        }
    }
});
