import Component from '@ember/component';

export default Component.extend({

    actions: {
        getContents: function(content) {
            $.ajax({
                url: this.get('contents_url') + content.path,
                type : 'get',
                success : response => {
                    console.log('response', response);
                    Ember.set(content, 'subContents', response);
                    Ember.set(content, 'isClicked', true);
                },
                error : error => {
                    console.log('error', error);
                }
            })
        }

    }
    
});
